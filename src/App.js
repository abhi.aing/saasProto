import './App.css';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Header from "./components/Header";
import Home from "./views/Home";
import Report from "./views/Report";

function App() {

return(
  <BrowserRouter>
  <Header />
  <Routes>
    <Route path="/" element={<Home />}/>
    <Route path="/report" element={<Report />}/>
  </Routes>
  </BrowserRouter>
)


}

export default App;
