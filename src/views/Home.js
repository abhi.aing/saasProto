import React, { useState } from 'react'

const Home = () => {
  const [selected, setSelected] = useState(0);


  const handleUpload = (e) =>{
    const files = e.target.files[0];
    
  }

  return (
    <div className="App container">
      <div className="home_page">
        <div>
          <div className="row p-3">
            <div className='col-md-2'>
              <label>Image 1</label>
              <img src="https://place-hold.it/150x150" className={selected===0?"border border-danger-subtle border-5":""} onClick={() => setSelected(0)} />
            </div>
            <div className='col-md-2'>
              <label>Image 2</label>
              <img src="https://place-hold.it/150x150" className={selected===1?"border border-danger-subtle border-5":""} onClick={() => setSelected(1)} />
            </div>
            <div className='col-md-2'>
              <label>Image 3</label>
              <img src="https://place-hold.it/150x150" className={selected===2?"border border-danger-subtle border-5":""} onClick={() => setSelected(2)} />
            </div>
            <div className='col-md-2'>
              <label>Image 4</label>
              <img src="https://place-hold.it/150x150" className={selected===3?"border border-danger-subtle border-5":""} onClick={()=>setSelected(3)} />
            </div>
          </div>
        </div>
        <div className='row p-3'>
          <div class="mb-3">
            <label for="formFile" class="form-label">Upload your own Image</label>
            <input class="form-control" type="file" id="formFile" />
          </div>
        </div>
      </div>
      <div className='row my-5'>
        <div className='col-md-4'>
          <button type="button" onClick={()=>window.location = "/report"} class="btn btn-danger block">Generate Report</button>
        </div>
      </div>
    </div>
  );

}

export default Home